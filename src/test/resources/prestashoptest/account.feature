Feature: Account management
  Creating an account

  Scenario Outline: Create an account, then check login and personal data
    Given I am on the AccountCreation page
    When I fill AccountCreation fields with gender <gender> firstName <first> lastName <last> password <password> email <mail> birthDate <birth> acceptPartnerOffers <offers> acceptPrivacyPolicy <privacy> acceptNewsletter <news> acceptGdpr <gpdr> and submit
    And I go to the Home page
    And I sign out
    Then I can successfully sign in with email <mail> password <password>
    And The displayed name is <display>
    And My personal information is gender <gender> firstName <first> lastName <last> email <mail> birthDate <birth> acceptPartnerOffers <offers> acceptPrivacyPolicy "no" acceptNewsletter <news> acceptGdpr "no"

    Examples: 
      | gender | first     | last        | display             | password     | mail               | birth        | offers | privacy | news  | gpdr  |
      | "M"    | "John"    | "Doe"       | "John Doe"          | "mypassword" | "jdoe@example.com" | "1990-01-02" | "no"   | "yes"   | "yes" | "yes" |
      | "F"    | "Jane"    | "Doe"       | "Jane Doe"          | "123&µ"      | "jane.doe@oo.com"  | "1992-01-02" | "yes"  | "yes"   | "yes" | "yes" |
      | "U"    | "Camille" | "Bertillon" | "Camille Bertillon" | "£→µ$¤11"    | "cb@gmail.com"     | "1968-02-10" | "yes"  | "yes"   | "no"  | "yes" |

  Scenario: Create an account with long name, then check login and personal data
    Given I am on the AccountCreation page
    When I fill AccountCreation fields with gender "U" firstName "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz" lastName "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz" password "012345678901234567890123456789012345678901234567890123456789" email "012345678901234567890123456789012345678901234567890123456789@oo.com" birthDate "2022-02-10" acceptPartnerOffers "yes" acceptPrivacyPolicy "yes" acceptNewsletter "yes" acceptGdpr "yes" and submit
    And I go to the Home page
    And I sign out
    Then I can successfully sign in with email "012345678901234567890123456789012345678901234567890123456789@oo.com" password "012345678901234567890123456789012345678901234567890123456789"
    And The displayed name is "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz" 
    And My personal information is gender "U" firstName "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz" lastName "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz" email "012345678901234567890123456789012345678901234567890123456789@oo.com" birthDate "2022-02-10" acceptPartnerOffers "yes" acceptPrivacyPolicy "no" acceptNewsletter "yes" acceptGdpr "no"

  Scenario: Create an account, then try to login with incorrect password
    Given I am on the AccountCreation page
    When I fill AccountCreation fields with gender "F" firstName "Alice" lastName "Noël" password "police" email "alice@bob.com" birthDate "1970-01-01" acceptPartnerOffers "yes" acceptPrivacyPolicy "yes" acceptNewsletter "yes" acceptGdpr "yes" and submit
    And I go to the Home page
    And I sign out
    And I go to the SignIn page
    And I fill SignIn fields with email "alice@bob.com" password "poluce" and submit
    Then The error message "Échec d'authentification" is displayed
